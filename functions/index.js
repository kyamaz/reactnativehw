const functions = require('firebase-functions');
const cors=require('cors')({origin:true})
const fs=require('fs')
const Uuid= require('uuid-v4')
const gcConfig={
    projectId:"ionic-698a5",
    keyFileName:"place.json"
}
const admin=require('firebase-admin')
const gcs= require('@google-cloud/storage')(gcConfig)
admin.initializeApp({
    credential:admin.credential.cert(require('./place.json'))
})
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
 exports.storeImage = functions.https.onRequest((request, response) => {
    cors(request, response, ()=>{
        if(!request.headers.authorization || !request.headers.authorization.startsWith("Bearer ")){
            console.error('unauthorized')
            return response.status(403).json({error:'unauthorized'})
        }
        let idToken;
        idToken= request.headers.authorization.split("Bearer ")[1];
        admin.auth().verifyIdToken(idToken).then(
            decodedToken=>{
                const body=JSON.parse(request.body)
                fs.writeFileSync('/tmp/uploaded-img.jpg', body.image, "base64", err=>{
                    console.error('cors fails', err)
                    return response.status(500).json({error:err})
                });
                const bucket= gcs.bucket('ionic-698a5.appspot.com')
                const uid=Uuid()
                bucket.upload('/tmp/uploaded-img.jpg',{
                    uploadType:"media",
                    destination:`/places/${uid}.jpg`,
                    metadata:{
                        metadata:{
                            contentType:'image/jpeg',
                            firebaseStorageDownloadTokens:uid
                        }
                    }
                } ,(err, file)=>{
                    if(!err){
                        return response.status(201).json({
                            imageUrl:`https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${encodeURIComponent(file.name)}?alt=media&token=${uid}`,
                            imagePath:`/places/${uid}.jpg`
                        })
                    }else{
                        console.error(err)
                        return response.status(500).json({error:err})

                    }
                })

            }
        )
        .catch(
            err=>{
                console.error('invalid token')
                return response.status(403).json({error:'invalid token'})
            }
        )
        
    })
   });

   exports.deleteImage=functions.database.ref("/places/{placeId}").onDelete(event=>{
       const placeData=event.val()
       const imagePath=placeData.imageName

       const bucket= gcs.bucket('ionic-698a5.appspot.com')
      return  bucket.file(imagePath).delete()

   })

