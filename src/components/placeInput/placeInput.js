import React, { Component } from "react";
import { View, Text, TextInput, Button, StyleSheet } from "react-native";
import {DefaultInput} from './../../components/UI/DefaultInput/DefaultInput';

const placeInput = (props) => (
  <DefaultInput  value={props.placeData.value} 
  valid={props.placeData.valid}
  touched={props.placeData.touched}
  onChangeText={props.onChangeText}
    placeholder="place input" />
);

export {placeInput as PlaceInput };
