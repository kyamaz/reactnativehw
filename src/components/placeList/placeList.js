import React from "react";
import { ScrollView, FlatList , StyleSheet} from "react-native";
import { ListItem } from "./../listItem/listItem";
const placeList = props => {
  return <FlatList style={styles.listContainer}
    data={props.places}
    renderItem={(info)=>(
        <ListItem 
            placeName={info.item.name} 
            placeImage={info.item.image}
            onItemPressed={()=>props.onItemSelected(info.item.key)} />
    )}/>;
};

const styles = StyleSheet.create({
  listContainer: {
    width: "100%",
    margin: 10
  }
});
export { placeList as PlaceList };
