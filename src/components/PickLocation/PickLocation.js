import React,{Component} from 'react'
import {View, StyleSheet, Text, Dimensions} from 'react-native'
import {ButtonMaterial} from './../../components/UI/ButtonMaterial/ButtonMaterial';
import MapView from 'react-native-maps'
import Geolocation from 'react-native-geolocation-service';

export class PickLocation extends Component{
    componentWillMount(){
        this.reset();
    }
    map;

    reset=_=>{
        this.setState({
            focusedLocation:{
                latitude:40.750330332,
                longitude:-73.989496042,
                latitudeDelta:0.0122,
                longitudeDelta:Dimensions.get('window').width/Dimensions.get('window').height * 0.0122
            },
            locationChose:false

        })
    }

    pickLocationHandler=event=>{
        const coords=event.nativeEvent.coordinate;
        this.map.animateToRegion({
            ...this.state.focusedLocation,
            latitude:coords.latitude,
            longitude:coords.longitude
        })
        this.setState(prevStat=>{
           return{ 
                focusedLocation:{
                    ...prevStat.focusedLocation,
                    latitude:coords.latitude,
                    longitude:coords.longitude
                },
                locationChose:true
            }
        })

        this.props.onLocationPick({
            longitude:coords.longitude,
            latitude:coords.latitude
        })
    }

    getLocationHandler = () => {
        Geolocation.getCurrentPosition(pos => {
          const coordsEvent = {
            nativeEvent: {
              coordinate: {
                latitude: pos.coords.latitude,
                longitude: pos.coords.longitude
              }
            }
          };
          this.pickLocationHandler(coordsEvent);
        },
      err => {
        console.error(err);
        alert("Fetching the Position failed, please pick one manually!");
      })      }


    render(){
        let marker;
        if(this.state.locationChose){
            marker=<MapView.Marker coordinate={this.state.focusedLocation}/>
        }
        return(
            <View style={styles.container}>
              <MapView
                initialRegion={this.state.focusedLocation}
                region={!this.state.locationChose?this.state.focusedLocation:null }
                onPress={this.pickLocationHandler}
                style={styles.map}
                ref={ref=>this.map=ref}> 
                {marker}
                </MapView>
                <View style={styles.buttonContainer}>
                    <ButtonMaterial color='#29aaf4'
                        onPress={this.getLocationHandler} >
                        locate
                    </ButtonMaterial>     
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        //...StyleSheet.absoluteFillObject,
       flexDirection:"row",
       flexWrap:"wrap",
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    map:{
        //...StyleSheet.absoluteFillObject,
        width:"80%", 
        height:150,
      //  backgroundColor:"#eee"
    },
    buttonContainer:{
        margin:5,
        width:"80%",
        }
})