import React from "react";
import { View, Text , StyleSheet, TouchableOpacity,Image } from "react-native";
const listItem = (props) => (
  <TouchableOpacity onPress={props.onItemPressed}>
    <View style={styles.placeItem}  >
     <Image resizeMode="cover" style={styles.placeImage}  source={props.placeImage}/>
      <Text>{props.placeName}</Text>
    </View>
  </TouchableOpacity>

);
const styles = StyleSheet.create({
  placeItem: {
    width: "100%",
    padding: 5,
    margin:2,
    backgroundColor: "#eee",
    flexDirection:"row",
    alignItems:"center"
  },
  placeImage:{
    marginRight:5,
    height:80,
    width: 95
  }
});
export { listItem as ListItem };
