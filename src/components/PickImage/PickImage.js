import React,{Component} from 'react'
import {View, StyleSheet, Image} from 'react-native'
import {ButtonMaterial} from './../../components/UI/ButtonMaterial/ButtonMaterial';
import ImagePicker from './../../../imagePicker'
export class PickImage extends Component{
    state={
        pickedImage:null
    }
    pickImageHandler=_=>{
        ImagePicker.showImagePicker({title: "Pick an Image", maxHeight:600, maxWidth:800}, res => {
            if (res.didCancel) {
              console.log("User cancelled!");
            } else if (res.error) {
              console.error("Error", res.error);
            } else {
              this.setState({
                pickedImage: { uri: res.uri,  }
              });
              this.props.onImagePicked({uri: res.uri, base64: res.data});
            }
          });
    }

    reset=_=>{
        this.setState({
            pickedImage:null
        })
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.placeholder}> 
                       <Image source={this.state.pickedImage} style={styles.placeimagePreview}/>
                </View>
                <View style={styles.buttonContainer}>
                    <ButtonMaterial color='#29aaf4'
                    onPress={this.pickImageHandler}>
                        pick image
                    </ButtonMaterial>   
                </View>
            </View>
  
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flexDirection:"row",
        flexWrap:"wrap",
         flex:1,
         alignItems:"center",
         justifyContent:"center"
     },
    placeholder:{
        width:"80%", 
        height:150,
        backgroundColor:"#eee"
    },
    imagePreview:{
        width:"100%",
        height:"100%",
        minHeight:150
    },
    buttonContainer:{
        margin:5,
        width:"80%"
    }
})