import {SET_PLACES, DELETE_PLACE, REMOVE_PLACE, PLACE_ADDED, START_PLACE_ADD} from './../actions/actionTypes'
const initialState={
    places:[],
    placeAdded:false
}


const reducer=(state =initialState, action)=>{
    switch(action.type){
/*           case ADD_PLACE:
          return {
              ...state,
              places:state.places.concat(
                {
                    name :action.payload, 
                    key:`${Math.random()}`,//https://github.com/wonday/react-native-pdf/issues/125
                    image :{
                        uri: action.image.uri
                    },
                    location:action.location,
                 }
            )
          }; */
          case SET_PLACES:
            return{
                ...state,
                places:action.places
            }

          case REMOVE_PLACE:
            return {
                ...state,
                places:state.places.filter((pl) => pl.key !== action.payload),

            }
          case START_PLACE_ADD:
          return {
            ...state,
            placeAdded:false
        }
          case PLACE_ADDED:
            return {
                ...state,
                placeAdded:true
            }
/*           case SELECT_PLACE:
          return {
              ...state,
              selectedPlace:state.places.find(place=>place.key=== action.payload)

          }
          case DESELECT_PLACE:
          return {
              ...state,
              selectedPlace:null
          } */

        default:
        return state
    }
};

export { reducer as placeReducer};