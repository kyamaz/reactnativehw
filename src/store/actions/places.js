import { SET_PLACES, REMOVE_PLACE, PLACE_ADDED, START_PLACE_ADD } from "./actionTypes";
import {uiStartLoading, uiStopLoading, getAuthToken} from './index'

export const addPlace=(placeName, location, image)=>{
 
    return dispatch=>{
        let authToken=undefined
        dispatch(uiStartLoading())
        dispatch(getAuthToken())
        .catch(
            err=>{
                console.error(err)
               alert(`no valid token found ${err}`)
            }  
           )
        .then(
            token=>{
                authToken=token
               return   fetch('https://us-central1-ionic-698a5.cloudfunctions.net/storeImage', {
                            method:"POST",
                            body:JSON.stringify({
                            image:image.base64
                        }),
                        headers:{
                            'Authorization':`Bearer ${token}`
                        }
                }
            )
        })
        .catch(
            err=>{
                console.error(err)
               alert(`store image failed ${err}`)
            }  
           )

/*         .then(res=>{
            console.log( 'base',res )
            if(!res){
                return
            }
        return    res.json()
        }) */
        .then(res=>{
            if(res.ok){
                return    res.json()
            }else{
                throw (new Error())
            }
        })
        .then( parsedRes=>{
            const payload={
                name:placeName,
                location,
                image:parsedRes.imageUrl,
                imagePath:parsedRes.imagePath
            }
            return    fetch(`https://ionic-698a5.firebaseio.com/places.json?auth=${authToken}`, {
                method:"POST",
                body:JSON.stringify(payload)
            })
            })
            .then(res=>{
                if(res.ok){
                    return    res.json()
                }else{
                    throw (new Error())
                }
            })
            .then( parsedRes=>{
                dispatch(uiStopLoading())
                dispatch(placeAdded())
            })
            .catch(
                err=>{
                    console.error(err)
                    alert('something went wrong', err)
                    dispatch(uiStopLoading())
                }        
            )
        }
}
export const startPlaceAdd=_=>{
    return {
        type: START_PLACE_ADD
    }
}
export const placeAdded=_=>{
    return {
        type:PLACE_ADDED
    }
}
export const deletePlace= key =>{
    return (dispatch)=>{

        dispatch(getAuthToken())
        .catch(
            err=>{
                console.error(err)
               alert(`no valid token found ${err}`)
            }  
           )
        .then(
            token=>{
                dispatch(removePlace(key))
               return  fetch(`https://ionic-698a5.firebaseio.com/places/${key}.json?auth=${token}`,{
               method:"DELETE"})
            }
        ) 
        .then(res=>{
            console.log( 'img', res )
            return     res.json()
        })
        .then( parsedRes=>{
        console.log( parsedRes)
    
        })
        .catch(
            err=>{
                console.error(err)
                alert('something went wrong', err)
                dispatch(uiStopLoading())
            }        
        )
    }
}
export const removePlace=key=>{
    return {
        type:REMOVE_PLACE,
        payload:key
    }
}
export const getPlaces=_=>{
    return (dispatch) =>{
        dispatch(getAuthToken())
        .then(
            token=>{
               return  fetch(`https://ionic-698a5.firebaseio.com/places.json?auth=${token}`)
            }
        ) 
        .catch(
         err=>{
             console.error(err)
            alert(`no valid token found ${err}`)
         }  
        )
        .then(res=>{
            if(res.ok){
                return    res.json()
            }else{
                throw (new Error())
            }
        }).then(
            parsedJson=>{
                let places=[]
    /*             for(let key in parsedJson){
                    places= [...places,...[ 
                        {  ...parsedJson[key],  image:{ uri: parsedJson[key].image}, key }
                    ]
                ]
                } */
                for (let key in parsedJson) {
                  places.push({
                    ...parsedJson[key],
                    image: {
                      uri: parsedJson[key].image
                    },
                    key
                  });
                }
                dispatch(setPlaces(places))
            }
        ).catch(
            err=>{
                console.error(err)
                alert('something went wrong')
            }        
        )
    }
}
export const setPlaces=(places)=>{
    return{
        type:SET_PLACES,
        places
    }

}
/* export const selectPlace= (key)=>{
    return{
        type: SELECT_PLACE,
        payload:key    
    }
}
export const deselectPlace= _ =>{
    return{
        type: DESELECT_PLACE,
    }
} */