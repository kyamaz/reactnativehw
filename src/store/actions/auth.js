import {TRY_AUTH, AUTH_SET_TOKEN, AUTH_REMOVE_TOKEN} from './actionTypes'
import {uiStartLoading, uiStopLoading} from './index'
import {startMainTabs} from './../../screens/MainTabs/startMainTabs'
import {AsyncStorage} from 'react-native'
import App from '../../../App'
import {Navigation} from 'react-native-navigation';


const key='AIzaSyCoOP-oZjcDI-3xCciOCEH1HL8Uj5ikkPs'

export const tryAuth=(authdata, authMode)=>{
    return dispatch=>{
        dispatch(uiStartLoading)
        let url=`https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${key}`
        if(authMode==='signup'){
         url=`https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${key}`
        }
        dispatch(authSignup(authdata, url))
    }
}

export const authSignup=(authdata, url)=>{
    return dispatch=>{
        fetch(url, {
        method:'POST',
        body:JSON.stringify({
            email:authdata.email,
            password:authdata.password,
            returnSecureToken:true
        }),
        headers:{
            "Content-Type":'application/json'
        }

    })
    .then(
        res=>res.json()
    )
    .then(
        parsedRes=>{
            dispatch(uiStopLoading)
            console.log( parsedRes)
            if(!parsedRes.idToken){
                alert('no token')
            }else{
                dispatch( authStoreToken( parsedRes.idToken, parsedRes.expiresIn, parsedRes.refreshToken ))
                startMainTabs();

            }
        }
    )
    .catch(
        err=>{
            dispatch(uiStopLoading)

            console.error(err)
            alert('auth failed')
        }
    )
    }
}

export const authStoreToken=(token, expiresIn, refreshToken)=>{
    return dispatch=>{
        const now= new Date()
        const expireDate=now.getTime()+ expiresIn *1000;

        dispatch(setAuth(token, expireDate));

        AsyncStorage.setItem('place:auth:token', token)
        AsyncStorage.setItem('place:auth:expireIn', expireDate.toString())
        AsyncStorage.setItem('place:auth:refreshToken',  refreshToken.toString())

    }
}
export const setAuth=(token, expireDate)=>{
    return{
        type:AUTH_SET_TOKEN,
        token,
        expireDate
    }
}

export const getAuthToken=()=>{
    return (dispatch, getState)=>{
        const p = new Promise((resolve, reject)=>{
            const token=getState().auth.token
            const expireDate=getState().auth.expireDate

            if(!!!token || new Date(expireDate) <= new Date()){
                let fetchToken;
                AsyncStorage.getItem( 'place:auth:token')
                .catch(
                    err=>{
                        reject()
                    }
                ).then(
                    storageToken=>{
                        if( !storageToken){
                            reject() 
                        }
                        fetchToken=storageToken
                      return  AsyncStorage.getItem( 'place:auth:expireIn')
                      
                    }

                ).then(
                    expireDate=>{
                        const parsedExpireDate= new Date(parseInt(expireDate));
                        const now =new Date()
                        if(parsedExpireDate>now){
                            dispatch(setAuth(fetchToken, parsedExpireDate))
                            resolve(fetchToken)
                        }else{
                            reject();
                        }

                    }
                )

            }else{
                resolve(token)
            }
        })

        return p
            .catch(e=>{
          return  AsyncStorage.getItem('place:auth:refreshToken').then(
                    refreshToken=>{
                    return    fetch(`https://securetoken.googleapis.com/v1/token?key=${key}`,{
                        method:"POST",
                        headers:{
                            "Content-Type":"application/x-www-form-urlencoded"
                        },
                        body:`grant_type=refresh_token&refresh_token=${refreshToken}`
                    })
                }
                ).then(res=>{
                    if(res.ok){
                        return    res.json()
                    }else{
                        throw (new Error())
                    }
                }).then(
                    parsedRes=>{
                        console.log("Refresh token!", !!parsedRes.id_token );

                        if(!!parsedRes.id_token){
                            dispatch(authStoreToken(parsedRes.id_token, parsedRes.expiresIn, parsedRes.refreshToken))
                            console.log("Refresh token worked!");
                            return parsedRes.refreshToken;
                        }else{
                            console.log("Refresh token unworked!");
                            dispatch(clearStorage())
                        }
                    }
                )
                .then(

                    token=>{
                        console.log(   token, 'toto')
                        if(!token){
                            throw new Error()
                        }else{
                            return token;
                        }
                    }
                )
        })
    }
}

export const authAutoSignin=_=>{
    return dispatch=>{
        dispatch(getAuthToken())
        .then( token=>{
            startMainTabs()
        })
        .catch(err=>{
            console.log(`failed to get token`)
        })
    }
}

export const clearStorage=_=>{
    return dispatch =>{
        AsyncStorage.removeItem('place:auth:token')
        AsyncStorage.removeItem('place:auth:expireIn')
       return  AsyncStorage.removeItem('place:auth:refreshToken')
    }
}
export const authLogout=token=>{
    return dispatch =>{
        dispatch( clearStorage()).then(()=> {
            Navigation.startSingleScreenApp({
                screen: {
                  screen:"place.AuthScreen",
                  title: "Login"
                }
              })
            
        })
        dispatch( authRemoveToken())
    
    }
}
export const authRemoveToken=()=>{
    return{
        type:AUTH_REMOVE_TOKEN
    }
}