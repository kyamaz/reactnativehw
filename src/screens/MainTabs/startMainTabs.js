import {Navigation} from 'react-native-navigation';
import {Platform} from "react-native";

import Icon from 'react-native-vector-icons/Ionicons'
const startMainTabs=()=>{
    Promise.all(
        [
            Icon.getImageSource( Platform.OS==='android'?'md-map':'ios-map', 30),
            Icon.getImageSource(Platform.OS==='android'?'md-share-alt':'ios-share', 30),
            Icon.getImageSource(Platform.OS==='android'?'md-menu':'ios-menu', 30)

        ]
    ).then(
        imgs=>{
            Navigation.startTabBasedApp({
     
                tabs:[
                    {
                        screen:'place.FindPlaceScreen',
                        label:'find place',
                        title:'find place',
                        icon: imgs[0],
                        navigatorButtons:{
                            leftButtons:[
                                {
                                    icon:imgs[2],
                                    title:'Menu',
                                    id:'sideDrawerToggle'
                                }
                            ]
                        }
                    },
                    {
                        screen:'place.SharePlaceScreen',
                        label:'share place',
                        title:'share place',
                        icon: imgs[1],
                        navigatorButtons:{
                            leftButtons:[
                                {
                                    icon:imgs[2],
                                    title:'Menu',
                                    id:'sideDrawerToggle'

                                }
                            ]
                        }
                    },
                    {
                        screen:'place.ArScreen',
                        label:'Ar test',
                        title:'Ar test',
                        icon: imgs[1],
                        /* navigatorButtons:{
                            leftButtons:[
                                {
                                    icon:imgs[2],
                                    title:'Menu',
                                    id:'sideDrawerToggle'

                                }
                            ]
                        } */
                    },
            
                ],
                tabsStyle:{
                    tabBarSelectedButtonColor:'orange'
                },
                appStyle:{
                    tabBarSelectedButtonColor:'orange'

                },
                drawer:{
                    left:{
                        screen:'place.SideDrawer'
                    }
                }
            })
        }
    )
  

   


   
}
export {startMainTabs}