import React, {Component} from 'react'
import {View, Text, Button, TextInput, StyleSheet, ScrollView, Image, ActivityIndicator} from 'react-native'
import {connect} from 'react-redux';
import { addPlace} from './../../store/actions/index'
import {ButtonMaterial} from './../../components/UI/ButtonMaterial/ButtonMaterial';
import {DefaultInput} from './../../components/UI/DefaultInput/DefaultInput';
import {MainText} from './../../components/UI/MainText/MainText';
import {HeadingText} from './../../components/UI/HeadingText/HeadingText';
import {PickImage} from './../../components/PickImage/PickImage';
import {PickLocation} from './../../components/PickLocation/PickLocation';
import {PlaceInput} from './../../components/placeInput/placeInput'
import {notEmpty, validator} from './../../utils/validation'
import {placeAdded, startPlaceAdd} from './../../store/actions/index'
class SharePlaceScreen extends Component{
    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent( this.onNavigatorEvent );
    }
    locPicker;
    imagePicker;
    static navigatorStyle={
        navBarButtonColor:'orange'
    }
    componentWillMount(){
        this.reset()
    }
    componentDidUpdate(){
        if(this.props.placeAdded){
            this.props.navigator.switchToTab({tabIndex:0 })
        }
    }
    reset=_=>{
        this.setState({
            controls:{
                placeName:{
                    value:"",
                    valid:false,
                    touched:false,
                    validationRules:{
                        notEmpty:true
                    }
                },
                location:{
                    value:null,
                    valid:true
                },
                image:{
                    value:null,
                    valid:false
                }
            },
        })
        if( this.imagePicker && this.locPicker){
            this.imagePicker.reset();
            this.locPicker.reset();
        }

    }
   placeChgHandler = val=>{
       this.setState(
        prevState => {
            return{
             controls:{
                 ...prevState.controls,
                 placeName:{
                     ...prevState.controls.placeName,
                     value:val,
                     valid:validator(val, prevState.controls.placeName.validationRules),
                     touched:true
                 }
             }
            }
         }
       )
    };

    onNavigatorEvent= event=>{
        if(event.type==='ScreenChangedEvent'){
            if(event.id==='willAppear'){
                this.props.onStartAddPlace()
            }
        }
        if(event.type==='NavBarButtonPress'){
            if(event.id==='sideDrawerToggle'){
                this.props.navigator.toggleDrawer({
                    side:'left'
                })
            }
        }
    }
    placeAddedHandler= () =>{
        this.props.onAddPlace(this.state.controls.placeName.value, this.state.controls.location.value, this.state.controls.image.value );
        this.reset()

     //   this.props.navigator.switchToTab({tabindex:0})
    }
    locationPickHandler=(location)=>{
        this.setState(prevState=>{
            return {
                controls:{
                    ...prevState.controls,
                    location:{
                        value:location,
                        valid:true
                    }
                }

            }
        })
    }
    imagePickedHandler= image =>{
        this.setState(prevState=>{
            return {
                controls:{
                    ...prevState.controls,
                    image:{
                        value:image,
                        valid:true
                    }
                }
            }
        })

    }

    render(){
        const defaultButton= ( 
        <ButtonMaterial color='#29aaf4'
            disabled={!this.state.controls.placeName.valid  || !this.state.controls.location.valid || !this.state.controls.image.valid }
            onPress={this.placeAddedHandler}>
            share place
        </ButtonMaterial>  )
        let submitButton= defaultButton

        if(this.props.isLoading){
            submitButton=<ActivityIndicator />
        }else{
            submitButton= defaultButton
        }
        return(
            <ScrollView>
                <View  style={styles.container}>
                    <MainText>
                        <HeadingText>
                            Share a place
                        </HeadingText>
                    </MainText>    
                    <PickImage onImagePicked={this.imagePickedHandler} 
                        ref={(ref)=>(this.imagePicker=ref)}/>
                    <PickLocation onLocationPick={this.locationPickHandler}  
                        ref={(ref)=> (this.locPicker=ref)}/>
                    <PlaceInput placeData={this.state.controls.placeName} 
                        onChangeText={this.placeChgHandler} />
                    <View style={styles.buttonContainer}>
                         {submitButton}
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center"
    },
    buttonContainer:{
        margin:5
    }
})
const mapStateToProps=state=>{
    return{
        isLoading:state.ui.isLoading,
        placeAdded:state.places.placeAdded 
    }
}
const mapDispatchToProps= dispatch=>{
    return{
        onAddPlace:(placeName, location, image)=>dispatch(addPlace(placeName, location, image)),
        onStartAddPlace:_=>dispatch(startPlaceAdd())
    }
}

const connectSharePlace= connect(mapStateToProps, mapDispatchToProps)(SharePlaceScreen)
export {connectSharePlace as SharePlaceScreen}