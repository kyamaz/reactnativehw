import React, {Component} from 'react'
import {View, Text, TouchableOpacity, StyleSheet, Animated} from 'react-native'
import {PlaceList} from './../../components/placeList/placeList'
import { connect} from 'react-redux'
import {getPlaces} from './../../store/actions'
class FindPlaceScreen extends Component{
    static navigatorStyle={
        navBarButtonColor:'orange'
    }
    state={
        placesLoaded:false,
        removeAnim: new Animated.Value(1),
        placesAnim:new Animated.Value(0)
    }
    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent( this.onNavigatorEvent );
    }

    onNavigatorEvent= event=>{
        if(event.type==='ScreenChangedEvent'){
            if(event.id==='willAppear'){
                this.props.onLoadPlaces();
            }
        }

        if(event.type==='NavBarButtonPress'){
            if(event.id==='sideDrawerToggle'){
                this.props.navigator.toggleDrawer({
                    side:'left'
                })
            }
        }
    }
    placesLoadedHandler=()=>{
        Animated.timing(this.state.placesAnim,{
            toValue:1,
            duration:500,
            useNativeDriver:true
        }).start()
    }
    placesSearchHandler=_=>{
        Animated.timing(this.state.removeAnim, {toValue:0, duration:500, useNativeDriver:true}).start(
            ()=>{
                this.setState({
                    placesLoaded:true
                });
                this.placesLoadedHandler();
            }
        )

    }
    onItemSelectedHandler=key=>{
        const selPlace=this.props.places.find(place=>place.key===key)
        this.props.navigator.push({
            screen:'place.placeDetailScreen',
            title:selPlace.name,
            passProps:{
                selectedPlace:selPlace
            }
        })
    }
    render(){
        let content= (
            <Animated.View style={{
                opacity:this.state.removeAnim,
                transform:[{
                    scale:this.state.removeAnim.interpolate({
                        inputRange:[0,1],
                        outputRange:[3,1]
                    })
                }]
            }}> 
                <TouchableOpacity onPress={this.placesSearchHandler}>
                    <View style={styles.searchButton}>  
                        <Text  style={styles.searchButtonText}>
                            Display Places
                        </Text>
                    </View>
                </TouchableOpacity>
            </Animated.View>

        )
        if(this.state.placesLoaded){
            content=(
                <Animated.View style={{
                    opacity:this.state.placesAnim
                }
                }>
                    <PlaceList places={this.props.places} onItemSelected={this.onItemSelectedHandler}/>

                </Animated.View>
            )
        }
        return (
            <View style={this.state.placesLoaded?null: styles.buttonContainer}>
            {content}

            </View>
        )
    }
}
const styles=StyleSheet.create({
    buttonContainer:{
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    },
    listContainer:{

    },
    searchButton:{
        borderColor:"orange",
        borderWidth:1,
        padding:20,
        borderRadius:3
    },
    searchButtonText:{
        color:"orange"
    }

})
const mapStateToPros=state=>{
    return{
        places:state.places.places
    }
}
const mapDispatchToProps=dispath=>{
    return{
        onLoadPlaces:()=>dispath(getPlaces())
    }
}
const connectFindPlace=connect(mapStateToPros, mapDispatchToProps)(FindPlaceScreen)

export {connectFindPlace as FindPlaceScreen}