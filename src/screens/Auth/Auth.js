import React, {Component} from 'react'
import {View, Text, Button, StyleSheet, TextInput, ImageBackground, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, ActivityIndicator} from 'react-native';
import {DefaultInput} from './../../components/UI/DefaultInput/DefaultInput';
import {HeadingText} from './../../components/UI/HeadingText/HeadingText';
import {MainText} from './../../components/UI/MainText/MainText';
import {ButtonMaterial} from './../../components/UI/ButtonMaterial/ButtonMaterial';
import {Dimensions} from 'react-native'
import backgroundImage from './../../assets/images/background.jpeg'
import {validator} from './../../utils/validation'
import {connect} from 'react-redux'
import { tryAuth, authAutoSignin } from './../../store/actions/index'
class AuthScreen extends Component{ 
    state={
        viewMode:Dimensions.get('window').height>500?"portrait":"landscape",
        authMode:'login',
        controls:{
            email:{
                value:'',
                valid:false,
                validationRules:{
                    isEmail:true
                },
                touched:false
            },
            password:{
                value:'',
                valid:false,
                validationRules:{
                    minLength:6
                },
                touched:false

            },
            confirmPassword:{
                value:'',
                valid:false,
                validationRules:{
                    equalTo:'password'
                },
                touched:false

            },
        }
    }
    constructor(props){ 
        super(props)
        Dimensions.addEventListener('change', this.updateStyle)
    }

    componentWillUnmount(){
        Dimensions.removeEventListener('change', this.updateStyle)
    }
    componentDidMount(){
        this.props.onAutoSignin()
    }
    updateStyle=(dims)=>{
        this.setState({
            viewMode:Dimensions.get('window').height>500?"portrait":"landscape"
        })
    }
    switchAuthModHandler=(prevState)=> {
        this.setState(prevState=>{
            return {
                authMode:prevState.authMode==='login'?'signup':'login'
            }
        })
    }
    authHandler=()=>{
        const authData={
            email:this.state.controls.email.value,
            password:this.state.controls.password.value,

        }
        this.props.onTryAuth(authData, this.state.authMode)
    }
    updateStateHandler=(key, value)=>{
        let connectedVal={}
        if(this.state.controls[key].validationRules.equalTo){
            const eq=this.state.controls[key].validationRules.equalTo;
            const eqVal=this.state.controls[eq].value;
            connectedVal={
                ...connectedVal,
                equalTo:eqVal
            }
        }
        if(key==='password'){
            const eq='password';
            const eqVal=this.state.controls[eq].value;
            connectedVal={
                ...connectedVal,
                equalTo:eqVal
            }
        }
        this.setState(prevState=>{
            return{
                controls:{
                    ...prevState.controls,
                    [key]:{
                        ...prevState.controls[key], 
                        value,
                        valid:validator(value, prevState.controls[key].validationRules, connectedVal),
                    touched:true,
                    confirmPassword:{
                        ...prevState.controls.confirmPassword,
                        valid:key ==="password"? 
                            validator(prevState.controls.confirmPassword.value, prevState.controls.confirmPassword.validationRules, connectedVal):
                            prevState.controls.confirmPassword.valid
                    }
                }
                }
            }
        })

    }
    render(){
        let headingText=undefined
        let confirmPwd=undefined
        let submitBtn=(
            <ButtonMaterial 
                disabled={!this.state.controls.confirmPassword.valid && this.state.authMode==='signup' || !this.state.controls.password.valid || !this.state.controls.email.valid  }
                color='#29aaf4' 
                onPress={this.authHandler}>
                Submit
            </ButtonMaterial>
        )

        if(this.props.isLoading){
            submitBtn=<ActivityIndicator/>
        }
        if( this.state.viewMode=='portrait'){
            headingText=(
                <MainText>
                <HeadingText>{this.state.authMode==='login'?'Login':'Sign in' }</HeadingText>
                </MainText>
            )
        }
        if(this.state.authMode==='signup'){
            confirmPwd=(
                <View   style={ this.state.viewMode=='landscape' ? styles.landscapePwdInput:styles.portraitPwdInput}>                       
                <DefaultInput placeholder='confirm password'
                     touched={this.state.controls.confirmPassword.touched}
                     value={this.state.controls.confirmPassword.value}
                     valid={this.state.controls.confirmPassword.valid}
                     secureTextEntry
                     onChangeText={(val)=>this.updateStateHandler('confirmPassword',val)}
                     style={styles.input}/>
                 </View>)
        }

        return(
            <ImageBackground 
            source={backgroundImage} 
            style={styles.bgImage}>
                <KeyboardAvoidingView style={styles.container}
                    behavior='padding'>
                    {headingText}
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View  style={styles.inputContainer}>
                            <ButtonMaterial color='#29aaf4'
                                onPress={this.switchAuthModHandler}>
                                { this.state.authMode==='login'?'Sign in':'Login' }
                            </ButtonMaterial>
                            <View  style={ styles.portraitPwdContainer}>
                                <DefaultInput placeholder='email'   
                                touched={this.state.controls.email.touched}
                                valid={this.state.controls.email.valid}
                                value={this.state.controls.email.value}
                                autoCapitalize='none'
                                autoCorrent={false}
                                keyboardType='email-address'
                                onChangeText={(val)=>this.updateStateHandler('email',val)}
                                style={styles.input} />
                            </View>
           
                            <View style={ this.state.viewMode=='portrait'|| this.state.authMode==='signup' ? styles.portraitPwdContainer:styles.landscapePwdInput}>                       
                                <View  style={ this.state.viewMode=='portrait' || this.state.authMode==='signup' ? styles.portraitPwdContainer:styles.landscapePwdInput}>                       
                                    <DefaultInput placeholder='password' 
                                        touched={this.state.controls.password.touched}
                                        valid={this.state.controls.password.valid}
                                        value={this.state.controls.password.value}
                                        secureTextEntry
                                        onChangeText={(val)=>this.updateStateHandler('password',val)}
                                        style={styles.input}/>
                                </View>
                                { confirmPwd } 
                            </View>    
                            { submitBtn }
                        </View>
                    </TouchableWithoutFeedback>
    
                </KeyboardAvoidingView>
            </ImageBackground>

        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    },
    bgImage:{
        width:"100%",
        flex:1
    },
    inputContainer:{
        width:"80%"

    },
    landscapePwdContainer:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    portraitPwdContainer:{
        flexDirection:"column",
        justifyContent:"flex-start",
        alignItems:"center",
        width:"80%"
    },

    landscapePwdInput:{
        width:"45%"
    },
    portraitPwdInput:{
        width:"100%"

    },
    input:{
        backgroundColor:'white',
        width:"80%"
    }
})

const mapStateToProps=state=>{
    return {
        isLoading: state.isLoading
    }
}
const mapDispatchToProps=dispatch=>{
    return {
        onTryAuth:(authData, authMode)=>dispatch(tryAuth(authData, authMode)),
        onAutoSignin:_=>dispatch(authAutoSignin())
    }
}

const connectedAuth=connect( mapStateToProps, mapDispatchToProps)(AuthScreen)
export { connectedAuth as AuthScreen}