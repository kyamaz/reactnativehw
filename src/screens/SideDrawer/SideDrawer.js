import React,{Component} from 'react'
import { View, Text, Dimensions, StyleSheet, TouchableOpacity , Platform} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import {connect} from 'react-redux'
import {authLogout} from './../../store/actions/index'

const mapDispatchToProps=dispatch=>{
    return {
        onLogOut:()=> dispatch(authLogout() )
    }
}
class SideDrawer extends Component{
    render(){
        return(
            <View style={[
                styles.container,
                { width:Dimensions.get('window').width * 0.8}] }>
                <TouchableOpacity onPress={this.props.onLogOut}>
                    <View style={styles.drawerItem}>
                        <Text>Logout </Text>
                        <Icon style={styles.drawerItemIcon} size={25}   
                        name={Platform.OS ==='android'?'md-log-out':'ios-log-out'}
                        color="#aaa" />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        paddingTop:40,
        backgroundColor:'white',
        flex:1
    },
    drawerItem:{
        flexDirection:"row",
        alignItems:"center",
        backgroundColor:"#eee",
        paddingTop:10,
        paddingBottom:10

    },
    drawerItemIcon:{
        marginLeft:10
    }
})

const connectSideDrawer= connect(null, mapDispatchToProps)(SideDrawer)
export {connectSideDrawer as SideDrawer}