import React, {Component} from "react";
import { Modal, View, Text, Image, Button, StyleSheet, TouchableOpacity, Platform, Dimensions } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import {connect} from 'react-redux'
import {deletePlace} from './../../store/actions/index';
import MapView from "react-native-maps"
class PlaceDetail extends Component{
  state={
    viewMode:"portrait"
  }
  constructor(props){
    super(props)

    Dimensions.addEventListener('change', this.updateStyle)
  }
  componentWillUnmount(){
    Dimensions.removeEventListener('change', this.updateStyle)

  }
  updateStyle=(dims)=>{
    this.setState({
      viewMode:Dimensions.get('window').height>500?'portrait':'landscape'
    })
  }
  placeDeletedHandler=_=>{
    this.props.onDeletePlace(this.props.selectedPlace.key);
    this.props.navigator.pop();
  }
  render(){
    return (
      <View style={[styles.container, this.state.viewMode==='landscape'? styles.landscapeContainer: styles.portraitContainer]}> 
       <View style={styles.placeDetailContainer}>
          <View  style={styles.subContainer}>
                <View>
                      <Image source={this.props.selectedPlace.image} style={styles.modalImage} />
                </View>
                <View>
                  <Text style={styles.modalText}>{this.props.selectedPlace.name}</Text>
                </View>
            </View> 
            <View  style={styles.subContainer}>
              <MapView initialRegion={{
                ...this.props.selectedPlace.location,
                latitudeDelta:0.0122,
                longitudeDelta:Dimensions.get('window').width/Dimensions.get('window').height * 0.0122
        
              }}
              
              style={styles.map}>
                <MapView.Marker coordinate={this.props.selectedPlace.location}/>
              </MapView>
          </View>
        </View>
        <View  style={styles.subContainer}>
                <Text style={styles.modalText}>{this.props.selectedPlace.name}</Text>
          </View>
        <View  style={styles.subContainer}>
            <TouchableOpacity onPress={this.placeDeletedHandler}>
              <View style={styles.iconContainer}>
                <Icon size={30}
                  name={Platform.OS ==='android'?'md-trash':'ios-trash'}
                color="red" />
              </View>
            </TouchableOpacity>
        </View>    
      </View>

    )
}
}
const styles = StyleSheet.create({
  container: {
    margin: 22,
    flex:1
  },
  placeDetailContainer:{
    flex:2
  },
  portraitContainer:{
    flexDirection:"column"
  },
  map:{
    ...StyleSheet.absoluteFillObject
  },
  landscapeContainer:{
    flexDirection:"row"
  },
  modalImage: {
    width: "100%",
    height: "100%"
  },
  modalText: {
    textAlign: "center",
    fontSize: 24
  },
  iconContainer: {
    alignItems: "center"
  },
  subContainer:{
    flex:1
  }

}); 

const mapDispatchToProps=dispatch=>{
  return{
    onDeletePlace:(key)=>dispatch(deletePlace(key))
  }
}
const connectedPlaceDetail= connect(null, mapDispatchToProps)(PlaceDetail)
export {  connectedPlaceDetail as PlaceDetailScreen };

/* props => {
  let modalContent = undefined; */

        /*         <Modal onRequestClose={props.onModalClosed}
              visible={ !!props.selectedPlace }
              animationType='slide'>
              <View style={styles.modalContainer}>
                  {modalContent }
                  <View>
                      <TouchableOpacity   onPress={props.onItemDeleted} >
                          <View style={styles.iconContainer}>
                              <Icon size={30} 
                              name='ios-trash' 
                              color='red'
                            />
                          </View>    
                        
                      </TouchableOpacity>
                      <Button  title="close" 
                       onPress={props.onModalClosed}/>   
                  </View>  
              </View>
          </Modal> */
/*   if (!!props.selectedPlace) {
    modalContent = ( 
      <View>
        <Image source={props.selectedPlace.image} style={styles.modalImage} />
        <Text style={styles.modalText}>{props.selectedPlace.name}</Text>
      </View>
    );
  } */
 
