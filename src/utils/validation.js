const validate=(val, rules, connectedVal)=>{
    let isValid=true;
    for(let prop in rules){
        if(prop ==='isEmail'){
            isValid=isValid && emailValidator(val)
        }else if(prop ==='minLength'){
            isValid=isValid && minLengthValidator(val, rules[prop])

        }else if(prop ==='equalTo'){
            isValid=isValid && equalValidator(val, connectedVal[prop])

        } else if(prop ==='notEmpty'){
            isValid=isValid && notEmpty(val)

        }else{
            console.log('default', prop )
            isValid=true
        }

    }
    return isValid
}
const emailValidator=val=>{

    return /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(val)

}

const minLengthValidator=(val, minLength)=>{
 return val.length>=minLength
}
const equalValidator=(val, checkVal)=>{
    return val===checkVal;
}
const notEmpty=val=>{
    return val.trim() !== ''
}

export {validate as validator}