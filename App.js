import {Navigation} from 'react-native-navigation';
import {Provider} from 'react-redux';
import {AuthScreen} from './src/screens/Auth/Auth';
import {SharePlaceScreen} from './src/screens/SharePlace/SharePlace';
import {FindPlaceScreen} from './src/screens/FindPlace/FindPlace';
import {configureStore} from './src/store/configureStore';
import {PlaceDetailScreen} from './src/screens/placeDetail/placeDetail'
import {SideDrawer} from './src/screens/SideDrawer/SideDrawer';
import {ArScreen} from './src/screens/Ar/Ar'

const store=configureStore()
//register screens
//config.h not found:https://github.com/facebook/react-native/issues/14382
Navigation.registerComponent('place.AuthScreen', () =>AuthScreen, store, Provider);
Navigation.registerComponent('place.SharePlaceScreen', () =>SharePlaceScreen, store, Provider);
Navigation.registerComponent('place.FindPlaceScreen', () =>FindPlaceScreen, store, Provider);
Navigation.registerComponent('place.placeDetailScreen', () =>PlaceDetailScreen, store, Provider);
Navigation.registerComponent('place.SideDrawer', () =>SideDrawer, store, Provider );
Navigation.registerComponent('place.ArScreen', () => ArScreen);

//start app nav

Navigation.startSingleScreenApp({
  screen:{
    screen:"place.AuthScreen",
    title:'login'
  }
})

/*  
FUCKING BUG ON IOS .DOESNOT WORK THIS WAY
export default () =>{
   
  return Navigation.startSingleScreenApp({
  screen: {
    screen:"place.AuthScreen",
    title: "Login"
  }
})
};  */



/* 
import React, {Component} from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import {PlaceList} from './src/components/placeList/placeList';
import {PlaceInput} from './src/components/placeInput/placeInput';
import placeImage from './src/assets/images/new-york.jpg'
import { PlaceDetail } from './src/components/placeDetail/placeDetail';
import {connect} from 'react-redux';
import {addPlace, deletePlace, selectPlace, deselectPlace} from './src/store/actions/index'
 class App extends React.Component {

  placeAddedHandler = placeName => {
    this.props.onAddPlace(placeName);
  };
  placeSelectedHandler = keyFromPlaceList => {
    this.props.onSelectPlace(keyFromPlaceList)
   };
   placeDelectedHandler = () => {
    this.props.onDeletePlace()
   };
   modalClosedHandler = () => {
    this.props.onDeselectPlace()
   };

  render() {
    return (
      <View style={styles.container}>
      <PlaceDetail selectedPlace={this.props.selectedPlace}
        onItemDeleted={this.placeDelectedHandler}
        onModalClosed={this.modalClosedHandler}  />
        <PlaceInput onPlaceAdded={this.placeAddedHandler} />
        <PlaceList places={this.props.places} 
          onItemSeleted={this.placeSelectedHandler}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding:50
  }
});

const mapStateToProps=state=>{
  //from configurestore, 

  console.log( state)

  return {
    places: state.places.places,
    selectedPlace: state.places.selectedPlace
  }
}
const mapDispatchToProps=dispatch=>{
  return {
    onAddPlace:(name)=>dispatch( addPlace(name) ),
    onDeletePlace:_=>dispatch(deletePlace()),
    onSelectPlace:(key)=>dispatch(selectPlace(key)),
    onDeselectPlace:_=>dispatch(deselectPlace())
  }
}
const connectedApp=connect(mapStateToProps, mapDispatchToProps )(App)
export {  connectedApp as App };
 */